# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    if n <= 1
        return 1
    else
        p = n * (n-1) + 1
          return p
    end
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    if m <= 1
         print(1, " ", 1, " ", 1, " ")
     end
        
    if m > 1
         i = 1
         p = m * (m-1) + 1
         d = p
        print(m, " ")
        print(m^3, " ")
            
        while i<=m
            print(d, " ")
            d = d + 2
            i = i + 1
        end
    end
end

function mostra_n(n)
    for i = 1:n
        imprime_impares_consecutivos(i)
        print('\n')
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()